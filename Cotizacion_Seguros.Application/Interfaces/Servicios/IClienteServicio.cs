﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Servicios
{
    public interface IClienteServicio
    {
        Task<Clientes> AgregarClienteAsync(Clientes cliente);

        Task<IEnumerable<Clientes>> ObtenerClientes();

        Task<Clientes> ObtenerClientePorId(Guid clienteId);
    }
}
