﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Servicios
{
    public interface IVehiculoServicio
    {
        Task<IEnumerable<Vehiculos>> ListarVehiculosPorClienteAsync(Guid clienteId);

        Task<Vehiculos> AgregarVehiculoAsync(Vehiculos vehiculo);
    }
}
