﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Servicios
{
    public interface ISeguroServicio
    {
        Task<Seguros> AgregarSeguroAsync(Seguros seguro);

        Task<IEnumerable<Asesores>> ObtenerAsesores();

        Task<ValoresAsegurados> AgregarValoresAseguradosAsync(ValoresAsegurados valoresAsegurados);

        Task<List<Seguros>> ObtenerSegurosPorVehiculosIdAsync(List<long> vehiculosId);

        Task<IReadOnlyList<Seguros>> ObtenerSegurosPorAsesorIdAsync(Guid asesorId);
    }
}
