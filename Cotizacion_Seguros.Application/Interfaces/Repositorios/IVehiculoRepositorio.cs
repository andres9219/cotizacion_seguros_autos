﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface IVehiculoRepositorio
    {
        Task<IEnumerable<Vehiculos>> ObtenerVehiculosPorClienteAsync(Guid clienteId);

        Task<Vehiculos> AgregarVehiculoAsync(Vehiculos vehiculo);
    }
}
