﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface IValoresRepositorio
    {
        Task<ValoresAsegurados> AgregarValoresAseguradosAsync(ValoresAsegurados valoresAsegurados);
    }
}
