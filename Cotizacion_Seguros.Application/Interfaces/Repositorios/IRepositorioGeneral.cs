﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface IRepositorioGeneral<T> where T : class
    {
        Task<IReadOnlyList<T>> ListarTodoAsync();

        Task<T> AgregarAsync(T entidad);

        Task ActualizarAsync(T entidad);

        Task EliminarAsync(T entidad);

        Task<int> CantidadDeRegistrosAsync(Expression<Func<T, bool>> expresionDeEspecificacion);

        Task<IReadOnlyList<T>> ListarAsync(Expression<Func<T, bool>> expresionDeEspecificacion);

        Task<IReadOnlyList<T>> ListarIncluyenteAsync(Expression<Func<T, object>> expresionDeInclusion, Expression<Func<T, bool>> expresionDeEspecificacion);
    }
}
