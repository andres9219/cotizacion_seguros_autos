﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface IClienteRepositorio
    {
        Task<Clientes> AgregarClienteAsync(Clientes cliente);

        Task<IEnumerable<Clientes>> ObtenerClientes();

        Task<Clientes> ObtenerClientePorId(Guid clienteId);
    }
}
