﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface ISeguroRepositorio
    {
        Task<Seguros> AgregarSeguroAsync(Seguros seguro);

        Task<List<Seguros>> ObtenerSegurosPorVehiculosIdAsync(List<long> vehiculosId);

        Task<IReadOnlyList<Seguros>> ObtenerSegurosPorAsesorIdAsync(Guid asesorId);
    }
}
