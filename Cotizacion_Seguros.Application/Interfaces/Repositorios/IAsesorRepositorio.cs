﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Interfaces.Repositorios
{
    public interface IAsesorRepositorio
    {
        Task<IEnumerable<Asesores>> ObtenerAsesores();
    }
}
