﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace Cotizacion_Seguros.Application.Utilidades
{
    public static class Negocio
    {
        public static List<SelectListItem> ObtenerEstadosCivilesActivos()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "Soltero" },
                new SelectListItem { Value = "2", Text = "Casado" }
            };
        }

        public static List<SelectListItem> ObtenerPolizas()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "SOAT" },
                new SelectListItem { Value = "2", Text = "TODO RIESGO" }
            };
        }

        public static List<SelectListItem> ObtenerAsesoresActivos(IEnumerable<Asesores> asesores)
        {
            return asesores.Select(a => new SelectListItem { Value = a.AsesorId.ToString(), Text = a.NombreCompleto }).ToList();
        }
    }
}