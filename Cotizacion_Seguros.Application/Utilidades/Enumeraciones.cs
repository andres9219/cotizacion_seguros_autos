﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cotizacion_Seguros.Application.Utilidades
{
    public class Enumeraciones
    {
        public enum EstadoCivil
        {
            Soltero = 1,
            Casado = 2
        }

        public enum Polizas
        {
            SOAT = 1,
            TODORIESGO = 2
        }
    }
}
