﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Logica.Servicios
{
    public class SeguroServicio : ISeguroServicio
    {
        private readonly ISeguroRepositorio _SeguroRepositorio;

        private readonly IAsesorRepositorio _AsesorRepositorio;

        private readonly IValoresRepositorio _ValoresRepositorio;

        public SeguroServicio(ISeguroRepositorio seguroRepositorio, IAsesorRepositorio asesorRepositorio, IValoresRepositorio valoresRepositorio)
        {
            _SeguroRepositorio = seguroRepositorio;
            _AsesorRepositorio = asesorRepositorio;
            _ValoresRepositorio = valoresRepositorio;
        }

        public async Task<Seguros> AgregarSeguroAsync(Seguros seguro)
        {
            return await _SeguroRepositorio.AgregarSeguroAsync(seguro);
        }

        public async Task<IEnumerable<Asesores>> ObtenerAsesores()
        {
            return await _AsesorRepositorio.ObtenerAsesores();
        }

        public async Task<ValoresAsegurados> AgregarValoresAseguradosAsync(ValoresAsegurados valoresAsegurados)
        {
            return await _ValoresRepositorio.AgregarValoresAseguradosAsync(valoresAsegurados);
        }

        public async Task<List<Seguros>> ObtenerSegurosPorVehiculosIdAsync(List<long> vehiculosId)
        {
            return await _SeguroRepositorio.ObtenerSegurosPorVehiculosIdAsync(vehiculosId);
        }

        public async Task<IReadOnlyList<Seguros>> ObtenerSegurosPorAsesorIdAsync(Guid asesorId)
        {
            return await _SeguroRepositorio.ObtenerSegurosPorAsesorIdAsync(asesorId);
        }
    }
}
