﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Logica.Servicios
{
    public class ClienteServicio : IClienteServicio
    {
        private readonly IClienteRepositorio _ClienteRepositorio;

        public ClienteServicio(IClienteRepositorio clienteRepositorio)
        {
            _ClienteRepositorio = clienteRepositorio;
        }

        public async Task<Clientes> AgregarClienteAsync(Clientes cliente)
        {
            return await _ClienteRepositorio.AgregarClienteAsync(cliente);
        }

        public async Task<IEnumerable<Clientes>> ObtenerClientes()
        {
            return await _ClienteRepositorio.ObtenerClientes();
        }

        public async Task<Clientes> ObtenerClientePorId(Guid clienteId)
        {
            return await _ClienteRepositorio.ObtenerClientePorId(clienteId);
        }
    }
}
