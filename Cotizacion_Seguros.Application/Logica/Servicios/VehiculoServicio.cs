﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Application.Logica.Servicios
{
    public class VehiculoServicio : IVehiculoServicio
    {
        private readonly IVehiculoRepositorio _VehiculoRepositorio;

        public VehiculoServicio(IVehiculoRepositorio vehiculoRepositorio)
        {
            _VehiculoRepositorio = vehiculoRepositorio;
        }

        public async Task<IEnumerable<Vehiculos>> ListarVehiculosPorClienteAsync(Guid clienteId)
        {
            return await _VehiculoRepositorio.ObtenerVehiculosPorClienteAsync(clienteId);
        }

        public async Task<Vehiculos> AgregarVehiculoAsync(Vehiculos vehiculo)
        {
            return await _VehiculoRepositorio.AgregarVehiculoAsync(vehiculo);
        }
    }
}
