﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotizacion_Seguros.Application.Entidades.Persistencia
{
    /// <summary>
    /// Representa un corredor de seguros en el sistema.
    /// </summary>
    [Table("Asesores")]
    public class Asesores
    {
        [Key()]
        public Guid AsesorId { get; set; }

        public long Identificacion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El asesor debe ser registrado con un nombre valido.")]
        public string NombreCompleto { get; set; }

        #region Entidades relacionadas

        public virtual ICollection<Seguros> SegurosAsociados { get; set; }

        #endregion
    }
}
