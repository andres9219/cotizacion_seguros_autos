﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotizacion_Seguros.Application.Entidades.Persistencia
{
    [Table("Vehiculos")]
    public class Vehiculos
    {
        [Key]
        public long VehiculoId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la placa del vehículo a registrar.")]
        public string Placa { get; set; }

        public string Modelo { get; set; }

        public int Anio { get; set; }

        public string SerialDeCarroceria { get; set; }

        public long ValorComercial { get; set; }

        #region Entidades relacionadas

        public Guid FkClienteId { get; set; }

        public Clientes Cliente { get; set; }

        public virtual ICollection<Seguros> Seguros { get; set; }

        #endregion
    }
}
