﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotizacion_Seguros.Application.Entidades.Persistencia
{
    /// <summary>
    /// Representa un cliente en el sistema.
    /// </summary>
    [Table("Cliente")]
    public class Clientes
    {
        [Key]
        public Guid ClienteId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar un número de identificación para el cliente.")]
        public string Identificacion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre completo del cliente.")]
        public string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la dirección del cliente.")]
        public string Direccion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar un número de contacto para el cliente.")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Debe ingresar la fecha de nacimiento del cliente.")]
        public DateTime FechaDeNacimiento { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Debe seleccionar el estado civul del cliente.")]
        public int EstadoCivilId { get; set; }

        #region Entidades relacionadas

        public virtual ICollection<Vehiculos> VehiculosAsociados { get; set; }

        #endregion
    }
}