﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cotizacion_Seguros.Application.Entidades.Persistencia
{
    /// <summary>
    /// Representa los valores a asegurar en una poliza todo riesgo.
    /// </summary>
    [Table("ValoresAsegurados")]
    public class ValoresAsegurados
    {
        [Key()]
        public long ValorAseguradoId { get; set; }

        public long MontoRadio { get; set; }

        public long MontoRines { get; set; }

        public long MontoAire { get; set; }

        #region Entidades relacionadas

        [Required()]
        public long FkSeguroId { get; set; }

        public Seguros SeguroAsociado { get; set; }

        #endregion
    }
}