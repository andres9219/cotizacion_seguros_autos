﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cotizacion_Seguros.Application.Entidades.Persistencia
{
    [Table("Seguros")]
    public class Seguros
    {
        [Key]
        public long SeguroId { get; set; }

        public int PolizaAsociadaId { get; set; }

        public double MontoAPagar { get; set; }

        #region Entidades relacionadas

        public long FkVehiculoId { get; set; }

        public Vehiculos VehiculoAsociado { get; set; }

        public Guid FkAsesorAsociadoId { get; set; }

        public Asesores AsesorAsociado { get; set; }

        public virtual ICollection<ValoresAsegurados> ValoresAsegurados { get; set; }

        #endregion
    }
}
