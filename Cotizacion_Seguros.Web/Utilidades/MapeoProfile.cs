﻿using AutoMapper;
using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Web.Models.Asesor;
using Cotizacion_Seguros.Web.Models.Cliente;
using Cotizacion_Seguros.Web.Models.Seguro;
using Cotizacion_Seguros.Web.Models.Vehiculo;

namespace Cotizacion_Seguros.Web.Utilidades
{
    public class MapeoProfile : Profile
    {
        public MapeoProfile()
        {
            CreateMap<Clientes, RegistrarCliente>().ReverseMap();
            CreateMap<Clientes, ConsultarCliente>().ReverseMap();
            CreateMap<Vehiculos, RegistrarVehiculo>().ReverseMap();
            CreateMap<Seguros, RegistrarSeguro>().ReverseMap();
            CreateMap<ValoresAsegurados, RegistrarValores>().ReverseMap();
            CreateMap<Asesores, ConsultarAsesor>().ReverseMap();
        }
    }
}
