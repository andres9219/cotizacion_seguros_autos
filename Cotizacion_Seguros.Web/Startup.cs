﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using Cotizacion_Seguros.Application.Logica.Servicios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using Cotizacion_Seguros.Infraestructure.Repositorios;
using Cotizacion_Seguros.Web.Utilidades;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Cotizacion_Seguros.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Repositorios
            services.AddScoped(typeof(IRepositorioGeneral<>), typeof(RepositorioGeneral<>));
            services.AddScoped<IClienteRepositorio, ClienteRepositorio>();
            services.AddScoped<IVehiculoRepositorio, VehiculoRepositorio>();
            services.AddScoped<IAsesorRepositorio, AsesorRepositorio>();
            services.AddScoped<ISeguroRepositorio, SeguroRepositorio>();
            services.AddScoped<IValoresRepositorio, ValoresRepositorio>();

            // Servicios
            services.AddScoped<IClienteServicio, ClienteServicio>();
            services.AddScoped<IVehiculoServicio, VehiculoServicio>();
            services.AddScoped<ISeguroServicio, SeguroServicio>();
            services.AddDbContextPool<SegurosDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SqlServerDev")));
            services.AddSingleton(new AutoMapper.MapperConfiguration(a => a.AddProfile(new MapeoProfile())).CreateMapper());


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
