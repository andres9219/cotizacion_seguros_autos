﻿using System.ComponentModel.DataAnnotations;

namespace Cotizacion_Seguros.Web.Models.Cliente
{
    public class ConsultarCliente : RegistrarCliente
    {
        [Display(Name = "Estado civil")]
        public string DescripcionEstadoCivil { get; set; }
    }
}
