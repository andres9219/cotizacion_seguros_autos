﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Models.Cliente
{
    public class RegistrarCliente
    {
        [HiddenInput(DisplayValue = false)]
        public Guid ClienteId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar un número de identificación para el cliente.")]
        [Display(Name = "Número de indentificación *")]
        public string Identificacion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar el nombre completo del cliente.")]
        [Display(Name = "Nombre completo *")]
        public string Nombre { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la dirección del cliente.")]
        [Display(Name = "Dirección *")]
        public string Direccion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar un número de contacto para el cliente.")]
        [Display(Name = "Telefono de contacto *")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Debe ingresar la fecha de nacimiento del cliente.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Fecha de nacimiento *")]
        public DateTime? FechaDeNacimiento { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Debe seleccionar el estado civul del cliente.")]
        [Display(Name = "Estado civil *")]
        public int EstadoCivilId { get; set; }
    }
}
