﻿using System.Collections.Generic;

namespace Cotizacion_Seguros.Web.Models.Cliente
{
    public class ListarCliente
    {
        public ICollection<ConsultarCliente> Clientes { get; set; }
    }
}
