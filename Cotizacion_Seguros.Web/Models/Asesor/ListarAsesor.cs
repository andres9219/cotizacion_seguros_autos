﻿using System.Collections.Generic;

namespace Cotizacion_Seguros.Web.Models.Asesor
{
    public class ListarAsesor
    {
        public ICollection<ConsultarAsesor> Asesores { get; set; }
    }
}
