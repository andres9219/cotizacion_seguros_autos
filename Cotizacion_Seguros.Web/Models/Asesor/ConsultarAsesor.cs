﻿using System;

namespace Cotizacion_Seguros.Web.Models.Asesor
{
    public class ConsultarAsesor
    {
        public Guid AsesorId { get; set; }

        public long Identificacion { get; set; }

        public string NombreCompleto { get; set; }
    }
}
