﻿using Cotizacion_Seguros.Web.Models.Vehiculo;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Models.Seguro
{
    public class RegistrarSeguro
    {
        public long SeguroId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FkClienteId { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Debe seleccionar un tipo de póliza.")]
        [Display(Name = "Póliza de seguro *")]
        public int PolizaAsociadaId { get; set; }

        [Display(Name = "Monto a pagar")]
        public double MontoAPagar { get; set; }

        [Display(Name = "Vehículo a asegurar *")]
        public long FkVehiculoId { get; set; }

        [Display(Name = "Asesor *")]
        public Guid FkAsesorAsociadoId { get; set; }

        public RegistrarValores ValoresAPagar { get; set; }

        public RegistrarVehiculo Vehiculo { get; set; }

        public void CalcularMontoAPagarSoat()
        {
            MontoAPagar = 0.01 * Vehiculo.ValorComercial;
        }

        public void CalcularMontoAPagarTodoRiesgo(DateTime fechaDeNacimientoDelCliente, int estadoCivilDelCliente)
        {
            var diasDelCliente = (DateTime.Now - fechaDeNacimientoDelCliente).TotalDays;
            var aniosDelCliente = Math.Truncate(diasDelCliente / 365);
            MontoAPagar = 0.01 * Vehiculo.ValorComercial;

            if (aniosDelCliente > 60)
            {
                MontoAPagar = (0.04 * Vehiculo.ValorComercial) + ObtenerValoresDeAccesorios();
            }
            else if (aniosDelCliente > 45)
            {
                MontoAPagar = (0.06 * Vehiculo.ValorComercial) + ObtenerValoresDeAccesorios();
            }

            if (estadoCivilDelCliente == EstadoCivil.Casado.GetHashCode())
            {
                MontoAPagar = (0.07 * Vehiculo.ValorComercial) + ObtenerValoresDeAccesorios();
            }
        }

        private double ObtenerValoresDeAccesorios()
        {
            return 0.1 * (ValoresAPagar.MontoAire + ValoresAPagar.MontoRines + ValoresAPagar.MontoRadio);
        }
    }
}
