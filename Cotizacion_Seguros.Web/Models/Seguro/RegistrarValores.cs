﻿using System.ComponentModel.DataAnnotations;

namespace Cotizacion_Seguros.Web.Models.Seguro
{
    public class RegistrarValores
    {
        public long ValorAseguradoId { get; set; }

        [Range(0, long.MaxValue, ErrorMessage = "El valor a asegurar no puede ser menor que cero (0).")]
        public long MontoRadio { get; set; }

        [Range(0, long.MaxValue, ErrorMessage = "El valor a asegurar no puede ser menor que cero (0).")]
        public long MontoRines { get; set; }

        [Range(0, long.MaxValue, ErrorMessage = "El valor a asegurar no puede ser menor que cero (0).")]
        public long MontoAire { get; set; }

        public long FkSeguroId { get; set; }
    }
}
