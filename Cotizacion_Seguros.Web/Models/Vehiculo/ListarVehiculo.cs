﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cotizacion_Seguros.Web.Models.Vehiculo
{
    public class ListarVehiculo
    {
        [Display(Name = "Cliente")]
        public string NombreDelCliente { get; set; }

        public ICollection<ConsultarVehiculo> Vehiculos { get; set; }

        [Display(Name = "Valor total de comisión")]
        public double MontoAcumulado { get; set; }
    }
}
