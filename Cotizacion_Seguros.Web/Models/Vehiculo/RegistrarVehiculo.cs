﻿using System.ComponentModel.DataAnnotations;

namespace Cotizacion_Seguros.Web.Models.Vehiculo
{
    public class RegistrarVehiculo
    {
        public long VehiculoId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Debe ingresar la placa del vehículo a registrar.")]
        public string Placa { get; set; }

        public string Modelo { get; set; }

        [Display(Name = "Año")]
        public int Anio { get; set; }

        [Display(Name = "Serial de carrocería")]
        public string SerialDeCarroceria { get; set; }

        [Display(Name = "Valor comercial")]
        public long ValorComercial { get; set; }
    }
}
