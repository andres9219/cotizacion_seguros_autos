﻿namespace Cotizacion_Seguros.Web.Models.Vehiculo
{
    public class ConsultarVehiculo : RegistrarVehiculo
    {
        public double MontoPagado { get; set; }

        public string DescripcionPoliza { get; set; }
    }
}
