﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using Cotizacion_Seguros.Application.Utilidades;
using Cotizacion_Seguros.Web.Models.Cliente;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IClienteServicio _ClienteServicio;

        private readonly IMapper _Mapper;

        public ClienteController(IClienteServicio clienteServicio, IMapper mapper)
        {
            _ClienteServicio = clienteServicio;
            _Mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Listar()
        {
            try
            {
                var clientesConsultados = new ListarCliente
                {
                    Clientes = ObtenerDescripcionEstadoCivilParaClientes(_Mapper.Map<IReadOnlyList<ConsultarCliente>>(await _ClienteServicio.ObtenerClientes())).ToList()
                };
                return View(clientesConsultados);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        public IActionResult Registrar()
        {
            ViewBag.EstadoCivil = new SelectList(Negocio.ObtenerEstadosCivilesActivos(), "Value", "Text");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registrar(RegistrarCliente registrarCliente)
        {
            try
            {
                if (!ModelState.IsValid && !Enum.IsDefined(typeof(EstadoCivil), registrarCliente.EstadoCivilId))
                {
                    ViewBag.EstadoCivil = new SelectList(Negocio.ObtenerEstadosCivilesActivos(), "Value", "Text");
                    return View(registrarCliente);
                }

                registrarCliente.ClienteId = Guid.NewGuid();
                await _ClienteServicio.AgregarClienteAsync(_Mapper.Map<Clientes>(registrarCliente));
                return RedirectToAction("Listar");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(registrarCliente);
            }

        }

        private IEnumerable<ConsultarCliente> ObtenerDescripcionEstadoCivilParaClientes(IEnumerable<ConsultarCliente> clientes)
        {
            clientes.ToList().ForEach(c => c.DescripcionEstadoCivil = Enum.GetName(typeof(EstadoCivil), c.EstadoCivilId));
            return clientes;
        }
    }
}