﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using Cotizacion_Seguros.Application.Utilidades;
using Cotizacion_Seguros.Web.Models.Seguro;
using Microsoft.AspNetCore.Mvc;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Controllers
{
    public class PolizaController : Controller
    {
        private readonly ISeguroServicio _SeguroServicio;

        private readonly IVehiculoServicio _VehiculoServicio;

        private readonly IClienteServicio _ClienteServicio;

        private readonly IMapper _Mapper;

        public PolizaController(ISeguroServicio seguroServicio, IVehiculoServicio vehiculoServicio, IClienteServicio clienteServicio, IMapper mapper)
        {
            _SeguroServicio = seguroServicio;
            _VehiculoServicio = vehiculoServicio;
            _ClienteServicio = clienteServicio;
            _Mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> RegistrarPoliza(Guid clienteId)
        {
            ViewBag.Asesores = Negocio.ObtenerAsesoresActivos(await _SeguroServicio.ObtenerAsesores());
            ViewBag.Polizas = Negocio.ObtenerPolizas();
            return View(new RegistrarSeguro { FkClienteId = clienteId });
        }

        [HttpPost]
        public async Task<IActionResult> RegistrarPoliza(RegistrarSeguro seguroARegistrar)
        {
            try
            {
                if (seguroARegistrar.FkClienteId == Guid.Empty || seguroARegistrar.FkAsesorAsociadoId == Guid.Empty || !Enum.IsDefined(typeof(Polizas), seguroARegistrar.PolizaAsociadaId))
                {
                    ViewBag.Asesores = Negocio.ObtenerAsesoresActivos(await _SeguroServicio.ObtenerAsesores());
                    ViewBag.Polizas = Negocio.ObtenerPolizas();
                    return View(seguroARegistrar);
                }

                var clienteAsociado = await _ClienteServicio.ObtenerClientePorId(seguroARegistrar.FkClienteId);
                var vehiculoARegistrar = _Mapper.Map<Vehiculos>(seguroARegistrar.Vehiculo);
                vehiculoARegistrar.FkClienteId = seguroARegistrar.FkClienteId;
                var vehiculoRegistrado = await _VehiculoServicio.AgregarVehiculoAsync(vehiculoARegistrar);
                var seguroPorRegistrar = _Mapper.Map<Seguros>(seguroARegistrar);
                seguroPorRegistrar.FkVehiculoId = vehiculoRegistrado.VehiculoId;

                if (seguroARegistrar.PolizaAsociadaId == Polizas.SOAT.GetHashCode())
                {
                    seguroARegistrar.CalcularMontoAPagarSoat();
                }
                else
                {
                    seguroARegistrar.CalcularMontoAPagarTodoRiesgo(clienteAsociado.FechaDeNacimiento, clienteAsociado.EstadoCivilId);
                }

                var seguroRegistrado = await _SeguroServicio.AgregarSeguroAsync(seguroPorRegistrar);

                if (seguroARegistrar.PolizaAsociadaId == Polizas.TODORIESGO.GetHashCode())
                {
                    var valoresAGuardar = _Mapper.Map<ValoresAsegurados>(seguroARegistrar.ValoresAPagar);
                    valoresAGuardar.FkSeguroId = seguroRegistrado.SeguroId;
                    var valoresGuardados = await _SeguroServicio.AgregarValoresAseguradosAsync(valoresAGuardar);
                }

                return RedirectToAction("Listar", "Cliente");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(seguroARegistrar);
            }
        }
    }
}