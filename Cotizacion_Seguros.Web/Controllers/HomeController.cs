﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cotizacion_Seguros.Web.Models;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using Cotizacion_Seguros.Web.Models.Asesor;
using AutoMapper;
using Cotizacion_Seguros.Web.Models.Vehiculo;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISeguroServicio _SeguroServicio;

        private readonly IMapper _Mapper;

        public HomeController(ISeguroServicio seguroServicio, IMapper mapper)
        {
            _Mapper = mapper;
            _SeguroServicio = seguroServicio;
        }

        public async Task<IActionResult> Index()
        {
            return View(new ListarAsesor()
            {
                Asesores = _Mapper.Map<ICollection<ConsultarAsesor>>(await _SeguroServicio.ObtenerAsesores())
            });
        }

        public async Task<IActionResult> ListarVehiculos(Guid asesorId, string nombreAsesor)
        {
            try
            {
                var seguros = await _SeguroServicio.ObtenerSegurosPorAsesorIdAsync(asesorId);
                return View(new ListarVehiculo()
                {
                    Vehiculos = seguros.Select(s => new ConsultarVehiculo
                    {
                        Anio = s.VehiculoAsociado.Anio,
                        Modelo = s.VehiculoAsociado.Modelo,
                        MontoPagado = s.MontoAPagar,
                        Placa = s.VehiculoAsociado.Placa,
                        ValorComercial = s.VehiculoAsociado.ValorComercial,
                        SerialDeCarroceria = s.VehiculoAsociado.SerialDeCarroceria,
                        DescripcionPoliza = Enum.GetName(typeof(Polizas), s.PolizaAsociadaId)
                    }).ToList(),
                    MontoAcumulado = seguros.Select(s => s.MontoAPagar * 0.15).Sum(),
                    NombreDelCliente = nombreAsesor
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
