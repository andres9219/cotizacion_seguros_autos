﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cotizacion_Seguros.Application.Interfaces.Servicios;
using Cotizacion_Seguros.Web.Models.Vehiculo;
using Microsoft.AspNetCore.Mvc;
using static Cotizacion_Seguros.Application.Utilidades.Enumeraciones;

namespace Cotizacion_Seguros.Web.Controllers
{
    public class VehiculoController : Controller
    {
        private readonly IVehiculoServicio _VehiculoServicio;

        private readonly IMapper _Mapper;

        private readonly ISeguroServicio _SeguroServicio;

        private readonly IClienteServicio _ClienteServicio;

        public VehiculoController(IVehiculoServicio vehiculoServicio, IMapper mapper, ISeguroServicio seguroServicio, IClienteServicio clienteServicio)
        {
            _Mapper = mapper;
            _VehiculoServicio = vehiculoServicio;
            _SeguroServicio = seguroServicio;
            _ClienteServicio = clienteServicio;
        }

        [HttpGet]
        public async Task<IActionResult> Listar(Guid guClienteId)
        {
            try
            {
                if (guClienteId != Guid.Empty)
                {
                    var vehiculos = await _VehiculoServicio.ListarVehiculosPorClienteAsync(guClienteId);
                    var clienteAsociado = await _ClienteServicio.ObtenerClientePorId(guClienteId);

                    if (vehiculos.Any())
                    {
                        var seguros = await _SeguroServicio.ObtenerSegurosPorVehiculosIdAsync(vehiculos.Select(v => v.VehiculoId).ToList());
                        return View(new ListarVehiculo()
                        {
                            Vehiculos = seguros.Select(s => new ConsultarVehiculo
                            {
                                Anio = s.VehiculoAsociado.Anio,
                                Modelo = s.VehiculoAsociado.Modelo,
                                MontoPagado = s.MontoAPagar,
                                Placa = s.VehiculoAsociado.Placa,
                                ValorComercial = s.VehiculoAsociado.ValorComercial,
                                SerialDeCarroceria = s.VehiculoAsociado.SerialDeCarroceria,
                                DescripcionPoliza = Enum.GetName(typeof(Polizas), s.PolizaAsociadaId)
                            }).ToList(),
                            NombreDelCliente = clienteAsociado.Nombre
                        });
                    }

                    return View(new ListarVehiculo() { NombreDelCliente = clienteAsociado.Nombre });
                }

                return View(new ListarVehiculo() { NombreDelCliente = "No existe el cliente indicado." });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}