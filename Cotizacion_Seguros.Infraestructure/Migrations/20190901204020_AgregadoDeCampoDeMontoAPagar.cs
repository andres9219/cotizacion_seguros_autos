﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cotizacion_Seguros.Infraestructure.Migrations
{
    public partial class AgregadoDeCampoDeMontoAPagar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "MontoAPagar",
                table: "Seguros",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MontoAPagar",
                table: "Seguros");
        }
    }
}
