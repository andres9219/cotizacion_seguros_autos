﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Cotizacion_Seguros.Infraestructure.Migrations
{
    public partial class CrearBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Asesores",
                columns: table => new
                {
                    AsesorId = table.Column<Guid>(nullable: false),
                    Identificacion = table.Column<long>(nullable: false),
                    NombreCompleto = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Asesores", x => x.AsesorId);
                });

            migrationBuilder.CreateTable(
                name: "Cliente",
                columns: table => new
                {
                    ClienteId = table.Column<Guid>(nullable: false),
                    Identificacion = table.Column<string>(nullable: false),
                    Nombre = table.Column<string>(nullable: false),
                    Direccion = table.Column<string>(nullable: false),
                    Telefono = table.Column<string>(nullable: false),
                    FechaDeNacimiento = table.Column<DateTime>(nullable: false),
                    EstadoCivilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.ClienteId);
                });

            migrationBuilder.CreateTable(
                name: "Vehiculos",
                columns: table => new
                {
                    VehiculoId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Placa = table.Column<string>(nullable: false),
                    Modelo = table.Column<string>(nullable: true),
                    Anio = table.Column<int>(nullable: false),
                    SerialDeCarroceria = table.Column<string>(nullable: true),
                    ValorComercial = table.Column<long>(nullable: false),
                    FkClienteId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehiculos", x => x.VehiculoId);
                    table.ForeignKey(
                        name: "FK_Vehiculos_Cliente_FkClienteId",
                        column: x => x.FkClienteId,
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Seguros",
                columns: table => new
                {
                    SeguroId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PolizaAsociadaId = table.Column<int>(nullable: false),
                    FkVehiculoId = table.Column<long>(nullable: false),
                    FkAsesorAsociadoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seguros", x => x.SeguroId);
                    table.ForeignKey(
                        name: "FK_Seguros_Asesores_FkAsesorAsociadoId",
                        column: x => x.FkAsesorAsociadoId,
                        principalTable: "Asesores",
                        principalColumn: "AsesorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Seguros_Vehiculos_FkVehiculoId",
                        column: x => x.FkVehiculoId,
                        principalTable: "Vehiculos",
                        principalColumn: "VehiculoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ValoresAsegurados",
                columns: table => new
                {
                    ValorAseguradoId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MontoRadio = table.Column<long>(nullable: false),
                    MontoRines = table.Column<long>(nullable: false),
                    MontoAire = table.Column<long>(nullable: false),
                    FkSeguroId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValoresAsegurados", x => x.ValorAseguradoId);
                    table.ForeignKey(
                        name: "FK_ValoresAsegurados_Seguros_FkSeguroId",
                        column: x => x.FkSeguroId,
                        principalTable: "Seguros",
                        principalColumn: "SeguroId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cliente_Identificacion",
                table: "Cliente",
                column: "Identificacion",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Seguros_FkAsesorAsociadoId",
                table: "Seguros",
                column: "FkAsesorAsociadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Seguros_FkVehiculoId",
                table: "Seguros",
                column: "FkVehiculoId");

            migrationBuilder.CreateIndex(
                name: "IX_ValoresAsegurados_FkSeguroId",
                table: "ValoresAsegurados",
                column: "FkSeguroId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehiculos_FkClienteId",
                table: "Vehiculos",
                column: "FkClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehiculos_Placa",
                table: "Vehiculos",
                column: "Placa",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ValoresAsegurados");

            migrationBuilder.DropTable(
                name: "Seguros");

            migrationBuilder.DropTable(
                name: "Asesores");

            migrationBuilder.DropTable(
                name: "Vehiculos");

            migrationBuilder.DropTable(
                name: "Cliente");
        }
    }
}
