﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cotizacion_Seguros.Infraestructure.Migrations
{
    public partial class ActualizacionDeCampoDeMontoAPagar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "MontoAPagar",
                table: "Seguros",
                nullable: false,
                oldClrType: typeof(long));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "MontoAPagar",
                table: "Seguros",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
