﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class ValoresRepositorio : RepositorioGeneral<ValoresAsegurados>, IValoresRepositorio
    {
        #region Constructores

        public ValoresRepositorio(SegurosDbContext contexto) : base(contexto)
        {

        }

        #endregion

        #region Metodos

        public async Task<ValoresAsegurados> AgregarValoresAseguradosAsync(ValoresAsegurados valoresAsegurados)
        {
            return await AgregarAsync(valoresAsegurados);
        }

        #endregion
    }
}