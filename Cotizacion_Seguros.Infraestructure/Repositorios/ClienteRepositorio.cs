﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class ClienteRepositorio : RepositorioGeneral<Clientes>, IClienteRepositorio
    {
        #region Constructores

        public ClienteRepositorio(SegurosDbContext contexto) : base(contexto)
        {

        }

        #endregion

        #region Metodos

        public async Task<Clientes> AgregarClienteAsync(Clientes cliente)
        {
            return await AgregarAsync(cliente);
        }

        public async Task<IEnumerable<Clientes>> ObtenerClientes()
        {
            return await ListarTodoAsync();
        }

        public async Task<Clientes> ObtenerClientePorId(Guid clienteId)
        {
            var clientes = await ListarAsync(c => c.ClienteId == clienteId);
            return clientes.FirstOrDefault();
        }

        #endregion
    }
}
