﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class VehiculoRepositorio : RepositorioGeneral<Vehiculos>, IVehiculoRepositorio
    {
        #region Constructores

        public VehiculoRepositorio(SegurosDbContext contexto) : base(contexto)
        {

        }

        #endregion

        #region Metodos
        
        public async Task<IEnumerable<Vehiculos>> ObtenerVehiculosPorClienteAsync(Guid clienteId)
        {
            return await ListarAsync(v => v.FkClienteId == clienteId);
        }

        public async Task<Vehiculos> AgregarVehiculoAsync(Vehiculos vehiculo)
        {
            return await AgregarAsync(vehiculo);
        }

        #endregion
    }
}
