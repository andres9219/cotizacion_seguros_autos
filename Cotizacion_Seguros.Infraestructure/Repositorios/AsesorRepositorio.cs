﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class AsesorRepositorio : RepositorioGeneral<Asesores>, IAsesorRepositorio
    {
        #region Constructores

        public AsesorRepositorio(SegurosDbContext contexto) : base(contexto)
        {

        }

        #endregion

        #region Metodos

        public async Task<IEnumerable<Asesores>> ObtenerAsesores()
        {
            return await ListarTodoAsync();
        }

        #endregion
    }
}
