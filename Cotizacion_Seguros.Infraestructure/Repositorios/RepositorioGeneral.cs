﻿using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class RepositorioGeneral<T> : IRepositorioGeneral<T> where T : class
    {
        #region Propiedades

        /// <summary>
        /// Contexto de datos del aplicativo
        /// </summary>
        protected readonly SegurosDbContext _Contexto;

        #endregion

        #region Constructores

        public RepositorioGeneral(SegurosDbContext contexto)
        {
            _Contexto = contexto;
        }

        #endregion

        public virtual async Task ActualizarAsync(T entidad)
        {
            _Contexto.Entry(entidad).State = EntityState.Modified;
            await _Contexto.SaveChangesAsync();
        }

        public virtual async Task<T> AgregarAsync(T entidad)
        {
            await _Contexto.AddAsync(entidad);
            await _Contexto.SaveChangesAsync();
            return entidad;
        }

        public virtual async Task<int> CantidadDeRegistrosAsync(Expression<Func<T, bool>> expresionDeEspecificacion)
        {
            return await _Contexto.Set<T>().AsNoTracking().Where(expresionDeEspecificacion).CountAsync();
        }

        public virtual async Task EliminarAsync(T entidad)
        {
            _Contexto.Set<T>().Remove(entidad);
            await _Contexto.SaveChangesAsync();
        }

        public virtual async Task<IReadOnlyList<T>> ListarTodoAsync()
        {
            return await _Contexto.Set<T>().AsNoTracking().ToListAsync();
        }

        public virtual async Task<IReadOnlyList<T>> ListarAsync(Expression<Func<T, bool>> expresionDeEspecificacion)
        {
            return await _Contexto.Set<T>().AsNoTracking().Where(expresionDeEspecificacion).ToListAsync();
        }

        public virtual async Task<IReadOnlyList<T>> ListarIncluyenteAsync(Expression<Func<T, object>> expresionDeInclusion, Expression<Func<T, bool>> expresionDeEspecificacion)
        {
            return await _Contexto.Set<T>().AsNoTracking().Where(expresionDeEspecificacion).Include(expresionDeInclusion).ToListAsync();
        }
    }
}
