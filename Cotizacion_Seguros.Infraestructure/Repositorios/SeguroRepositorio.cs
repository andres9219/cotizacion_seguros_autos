﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Cotizacion_Seguros.Application.Interfaces.Repositorios;
using Cotizacion_Seguros.Infraestructure.ContextoDeDatos;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Cotizacion_Seguros.Infraestructure.Repositorios
{
    public class SeguroRepositorio : RepositorioGeneral<Seguros>, ISeguroRepositorio
    {
        #region Constructores

        public SeguroRepositorio(SegurosDbContext contexto) : base(contexto)
        {

        }

        #endregion

        #region Metodos

        public async Task<Seguros> AgregarSeguroAsync(Seguros seguro)
        {
            return await AgregarAsync(seguro);
        }

        public async Task<List<Seguros>> ObtenerSegurosPorVehiculosIdAsync(List<long> vehiculosId)
        {
            List<Seguros> segurosConsultados = new List<Seguros>();

            foreach (var item in vehiculosId)
            {
                var lista = await ListarIncluyenteAsync(s => s.VehiculoAsociado, s => s.FkVehiculoId == item);
                segurosConsultados.AddRange(lista.ToList());
            }

            return segurosConsultados.ToList();
        }

        public async Task<IReadOnlyList<Seguros>> ObtenerSegurosPorAsesorIdAsync(Guid asesorId)
        {
            return await ListarIncluyenteAsync(s => s.VehiculoAsociado, s => s.FkAsesorAsociadoId == asesorId);
        }

        #endregion
    }
}