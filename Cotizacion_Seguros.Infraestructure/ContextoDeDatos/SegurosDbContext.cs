﻿using Cotizacion_Seguros.Application.Entidades.Persistencia;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Cotizacion_Seguros.Infraestructure.ContextoDeDatos
{
    public class SegurosDbContext : DbContext
    {
        #region Propiedades

        public virtual DbSet<Seguros> Seguros { get; set; }

        public virtual DbSet<Asesores> Asesores { get; set; }

        public virtual DbSet<ValoresAsegurados> ValoresAsegurados { get; set; }

        public virtual DbSet<Clientes> Clientes { get; set; }

        public virtual DbSet<Vehiculos> Vehiculos { get; set; }

        #endregion

        #region Constructores

        public SegurosDbContext(DbContextOptions<SegurosDbContext> opciones) : base(opciones)
        {

        }

        #endregion

        protected override void OnModelCreating(ModelBuilder constructorDelModelo)
        {
            // Configuracion de relaciones para entidades.

            constructorDelModelo.Entity<Clientes>().HasIndex(c => c.Identificacion).IsUnique();
            constructorDelModelo.Entity<Vehiculos>().HasIndex(v => v.Placa).IsUnique();

            constructorDelModelo.Entity<Clientes>().HasMany(c => c.VehiculosAsociados)
                                                   .WithOne(v => v.Cliente)
                                                   .HasForeignKey(v => v.FkClienteId)
                                                   .OnDelete(DeleteBehavior.Restrict);

            constructorDelModelo.Entity<Asesores>().HasMany(c => c.SegurosAsociados)
                                                   .WithOne(v => v.AsesorAsociado)
                                                   .HasForeignKey(v => v.FkAsesorAsociadoId)
                                                   .OnDelete(DeleteBehavior.Restrict);

            constructorDelModelo.Entity<Seguros>().HasMany(c => c.ValoresAsegurados)
                                                  .WithOne(v => v.SeguroAsociado)
                                                  .HasForeignKey(v => v.FkSeguroId)
                                                  .OnDelete(DeleteBehavior.Restrict);

            constructorDelModelo.Entity<Vehiculos>().HasMany(c => c.Seguros)
                                                    .WithOne(v => v.VehiculoAsociado)
                                                    .HasForeignKey(v => v.FkVehiculoId)
                                                    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
