USE [SegurosAutos]
GO
/****** Object:  Table [dbo].[Asesores]    Script Date: 01/09/2019 23:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asesores](
	[AsesorId] [uniqueidentifier] NOT NULL,
	[Identificacion] [bigint] NOT NULL,
	[NombreCompleto] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Asesores] PRIMARY KEY CLUSTERED 
(
	[AsesorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 01/09/2019 23:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[ClienteId] [uniqueidentifier] NOT NULL,
	[Identificacion] [nvarchar](450) NOT NULL,
	[Nombre] [nvarchar](max) NOT NULL,
	[Direccion] [nvarchar](max) NOT NULL,
	[Telefono] [nvarchar](max) NOT NULL,
	[FechaDeNacimiento] [datetime2](7) NOT NULL,
	[EstadoCivilId] [int] NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[ClienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Seguros]    Script Date: 01/09/2019 23:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seguros](
	[SeguroId] [bigint] IDENTITY(1,1) NOT NULL,
	[PolizaAsociadaId] [int] NOT NULL,
	[FkVehiculoId] [bigint] NOT NULL,
	[FkAsesorAsociadoId] [uniqueidentifier] NOT NULL,
	[MontoAPagar] [float] NOT NULL,
 CONSTRAINT [PK_Seguros] PRIMARY KEY CLUSTERED 
(
	[SeguroId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValoresAsegurados]    Script Date: 01/09/2019 23:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValoresAsegurados](
	[ValorAseguradoId] [bigint] IDENTITY(1,1) NOT NULL,
	[MontoRadio] [bigint] NOT NULL,
	[MontoRines] [bigint] NOT NULL,
	[MontoAire] [bigint] NOT NULL,
	[FkSeguroId] [bigint] NOT NULL,
 CONSTRAINT [PK_ValoresAsegurados] PRIMARY KEY CLUSTERED 
(
	[ValorAseguradoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vehiculos]    Script Date: 01/09/2019 23:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehiculos](
	[VehiculoId] [bigint] IDENTITY(1,1) NOT NULL,
	[Placa] [nvarchar](450) NOT NULL,
	[Modelo] [nvarchar](max) NULL,
	[Anio] [int] NOT NULL,
	[SerialDeCarroceria] [nvarchar](max) NULL,
	[ValorComercial] [bigint] NOT NULL,
	[FkClienteId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Vehiculos] PRIMARY KEY CLUSTERED 
(
	[VehiculoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Asesores] ([AsesorId], [Identificacion], [NombreCompleto]) VALUES (N'4443c28c-6fd8-4387-9f8f-0609ae9a6403', 28564345, N'Silvia Rodriguez')
INSERT [dbo].[Asesores] ([AsesorId], [Identificacion], [NombreCompleto]) VALUES (N'2dd5397c-6198-4c72-bc34-0969028c98d5', 10983456432, N'Carlos Lopez')
INSERT [dbo].[Asesores] ([AsesorId], [Identificacion], [NombreCompleto]) VALUES (N'78de54ad-932f-42d4-b606-2c9823650869', 10989992342, N'Julian Garcia')
INSERT [dbo].[Asesores] ([AsesorId], [Identificacion], [NombreCompleto]) VALUES (N'8b830866-f112-48cf-a0f3-d410885a67aa', 10998456433, N'Andrea Gutierrez')
INSERT [dbo].[Cliente] ([ClienteId], [Identificacion], [Nombre], [Direccion], [Telefono], [FechaDeNacimiento], [EstadoCivilId]) VALUES (N'0e4d89d4-f5a9-462b-adec-1aeef6c87e60', N'121423424', N'Andres Rodriguez', N'Calle 34', N'43234234234', CAST(N'2019-03-07T00:00:00.0000000' AS DateTime2), 2)
INSERT [dbo].[Cliente] ([ClienteId], [Identificacion], [Nombre], [Direccion], [Telefono], [FechaDeNacimiento], [EstadoCivilId]) VALUES (N'f2415534-5fde-445b-b9e6-1bea00532870', N'23456765', N'Sandra Arias', N'Cra 34 No. 34 - 34', N'3145656789', CAST(N'2013-01-03T00:00:00.0000000' AS DateTime2), 2)
SET IDENTITY_INSERT [dbo].[Seguros] ON 

INSERT [dbo].[Seguros] ([SeguroId], [PolizaAsociadaId], [FkVehiculoId], [FkAsesorAsociadoId], [MontoAPagar]) VALUES (1, 1, 1, N'4443c28c-6fd8-4387-9f8f-0609ae9a6403', 3000000)
INSERT [dbo].[Seguros] ([SeguroId], [PolizaAsociadaId], [FkVehiculoId], [FkAsesorAsociadoId], [MontoAPagar]) VALUES (3, 2, 3, N'2dd5397c-6198-4c72-bc34-0969028c98d5', 1465000.0000000002)
INSERT [dbo].[Seguros] ([SeguroId], [PolizaAsociadaId], [FkVehiculoId], [FkAsesorAsociadoId], [MontoAPagar]) VALUES (4, 1, 4, N'4443c28c-6fd8-4387-9f8f-0609ae9a6403', 1560000)
INSERT [dbo].[Seguros] ([SeguroId], [PolizaAsociadaId], [FkVehiculoId], [FkAsesorAsociadoId], [MontoAPagar]) VALUES (5, 2, 5, N'2dd5397c-6198-4c72-bc34-0969028c98d5', 3400000)
SET IDENTITY_INSERT [dbo].[Seguros] OFF
SET IDENTITY_INSERT [dbo].[ValoresAsegurados] ON 

INSERT [dbo].[ValoresAsegurados] ([ValorAseguradoId], [MontoRadio], [MontoRines], [MontoAire], [FkSeguroId]) VALUES (1, 0, 0, 0, 1)
INSERT [dbo].[ValoresAsegurados] ([ValorAseguradoId], [MontoRadio], [MontoRines], [MontoAire], [FkSeguroId]) VALUES (2, 200000, 150000, 300000, 3)
INSERT [dbo].[ValoresAsegurados] ([ValorAseguradoId], [MontoRadio], [MontoRines], [MontoAire], [FkSeguroId]) VALUES (3, 45000, 65000, 350000, 5)
SET IDENTITY_INSERT [dbo].[ValoresAsegurados] OFF
SET IDENTITY_INSERT [dbo].[Vehiculos] ON 

INSERT [dbo].[Vehiculos] ([VehiculoId], [Placa], [Modelo], [Anio], [SerialDeCarroceria], [ValorComercial], [FkClienteId]) VALUES (1, N'PXM80D', N'SD4', 2019, N'ASDF34567', 300000000, N'0e4d89d4-f5a9-462b-adec-1aeef6c87e60')
INSERT [dbo].[Vehiculos] ([VehiculoId], [Placa], [Modelo], [Anio], [SerialDeCarroceria], [ValorComercial], [FkClienteId]) VALUES (3, N'PWZ56Y', N'EER4', 2109, N'RERTETETR66575', 20000000, N'0e4d89d4-f5a9-462b-adec-1aeef6c87e60')
INSERT [dbo].[Vehiculos] ([VehiculoId], [Placa], [Modelo], [Anio], [SerialDeCarroceria], [ValorComercial], [FkClienteId]) VALUES (4, N'ABC90D', N'EER4', 1990, N'RERTETETR66575', 45900000, N'f2415534-5fde-445b-b9e6-1bea00532870')
INSERT [dbo].[Vehiculos] ([VehiculoId], [Placa], [Modelo], [Anio], [SerialDeCarroceria], [ValorComercial], [FkClienteId]) VALUES (5, N'WEQ80W', N'EER4', 2005, N'RERTETETR66575', 348000000, N'f2415534-5fde-445b-b9e6-1bea00532870')
SET IDENTITY_INSERT [dbo].[Vehiculos] OFF
ALTER TABLE [dbo].[Seguros]  WITH CHECK ADD  CONSTRAINT [FK_Seguros_Asesores_FkAsesorAsociadoId] FOREIGN KEY([FkAsesorAsociadoId])
REFERENCES [dbo].[Asesores] ([AsesorId])
GO
ALTER TABLE [dbo].[Seguros] CHECK CONSTRAINT [FK_Seguros_Asesores_FkAsesorAsociadoId]
GO
ALTER TABLE [dbo].[Seguros]  WITH CHECK ADD  CONSTRAINT [FK_Seguros_Vehiculos_FkVehiculoId] FOREIGN KEY([FkVehiculoId])
REFERENCES [dbo].[Vehiculos] ([VehiculoId])
GO
ALTER TABLE [dbo].[Seguros] CHECK CONSTRAINT [FK_Seguros_Vehiculos_FkVehiculoId]
GO
ALTER TABLE [dbo].[ValoresAsegurados]  WITH CHECK ADD  CONSTRAINT [FK_ValoresAsegurados_Seguros_FkSeguroId] FOREIGN KEY([FkSeguroId])
REFERENCES [dbo].[Seguros] ([SeguroId])
GO
ALTER TABLE [dbo].[ValoresAsegurados] CHECK CONSTRAINT [FK_ValoresAsegurados_Seguros_FkSeguroId]
GO
ALTER TABLE [dbo].[Vehiculos]  WITH CHECK ADD  CONSTRAINT [FK_Vehiculos_Cliente_FkClienteId] FOREIGN KEY([FkClienteId])
REFERENCES [dbo].[Cliente] ([ClienteId])
GO
ALTER TABLE [dbo].[Vehiculos] CHECK CONSTRAINT [FK_Vehiculos_Cliente_FkClienteId]
GO
